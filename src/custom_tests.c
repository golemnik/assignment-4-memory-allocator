#include "custom_tests.h"
#include "mem.h"
#include "mem_internals.h"

#include <stdio.h>

#define BYTE_SIZE (sizeof(uint8_t))
#define heap_error(...) debug_heap(stderr, __VA_ARGS__)

void debug(const char* fmt, ... );

void run(void (*test_func)(void *), const char* test_name) {
    void * heap = heap_init(0);
    debug(test_name);
    debug("... ");
    test_func(heap);
    heap_error(heap);
    heap_term();
    debug("OK\n");
}

void free_single_block (void *heap) {
    void *block1 = _malloc(BYTE_SIZE);
    heap_error(heap);
    _free(block1);
}

void free_multiple_blocks (void *heap) {
    _malloc(BYTE_SIZE * 100);
    _malloc(BYTE_SIZE * 100);
    void *block1 = _malloc(BYTE_SIZE * 300);
    void *block2 = _malloc(BYTE_SIZE * 500);
    _malloc(BYTE_SIZE * 100);
    void *block3 = _malloc(BYTE_SIZE * 400);
    heap_error(heap);
    _free(block1);
    heap_error(heap);
    _free(block2);
    heap_error(heap);
    _free(block3);
}

void extend_region_ordered (void *heap) {
    void *block1 = _malloc(REGION_MIN_SIZE);
    heap_error(heap);
    void *block2 = _malloc(REGION_MIN_SIZE*2+1);
    heap_error(heap);
    void *block3 = _malloc(REGION_MIN_SIZE);
    _free(block1);
    heap_error(heap);
    _free(block2);
    heap_error(heap);
    _free(block3);
}

void extend_region_unordered (void *heap) {
    void *blocked_block_addr = heap + REGION_MIN_SIZE;
    void* blocked_block = mmap(blocked_block_addr, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);
    void *block1 = _malloc(REGION_MIN_SIZE + BYTE_SIZE*32);
    heap_error(heap);
    void *block2 = _malloc(REGION_MIN_SIZE + BYTE_SIZE*32);
    heap_error(heap);
    _free(block1);
    heap_error(heap);
    _free(block2);
    heap_error(heap);
    _free(blocked_block);

}


void tests () {
    run(free_single_block, "Test 1. Free single block");
    run(free_multiple_blocks, "Test 2. Free multiple blocks");
    run(extend_region_ordered, "Test 3. Extend region ordered");
    run(extend_region_unordered, "Test 4. Extend region unordered");
}


