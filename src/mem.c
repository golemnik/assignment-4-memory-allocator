#define _DEFAULT_SOURCE

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define BLOCK_HEADER_SIZE offsetof(struct block_header, contents)

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline size_t aligned_size( size_t query );
extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void try_merge_with_all_next(struct block_header* block);


static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

static struct region alloc_region  ( void const * addr, size_t query ) { // ---------------------------------- stage 1
  size_t size = region_actual_size (query + offsetof(struct block_header, contents) );
  bool extends = true;
  void* mapped = map_pages(addr, size, MAP_FIXED_NOREPLACE);
  if (MAP_FAILED == mapped) {
    mapped = map_pages(addr, size, 0);
    extends = false;
  }
  struct region r = (struct region) {
    .addr = MAP_FAILED == mapped ? NULL : mapped,
    .size = size,
    .extends = extends
  };

  if (MAP_FAILED != mapped) {
    block_init(mapped, (block_size) { .bytes = size }, NULL);
  }
  return r;
}

static void* block_after( struct block_header const* block );
static bool blocks_continuous ( struct block_header const* fst, struct block_header const* snd );

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

void heap_term( ) { // ---------------------------------- stage 8
  void* heap_head = HEAP_START;

  while (NULL != heap_head) {
    struct block_header* block  = heap_head;
    block_size size = size_from_capacity( block->capacity );
    while (NULL != block->next && blocks_continuous(block, block->next)) {
        block = block->next;
        block_size sz = size_from_capacity( block->capacity );
        size.bytes += sz.bytes;
    }
    block = block->next;
    if (0 != munmap(heap_head, size.bytes)) {
        err("Unable to munmap(%p, %ld) errno=%d %s\n", heap_head, size.bytes, errno, strerror(errno));
    }
    heap_head = block;
  }
}

#define BLOCK_MIN_CAPACITY 24

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) { // ---------------------------------- stage 5
  size_t aligned_query = aligned_size(query > BLOCK_MIN_CAPACITY ? query : BLOCK_MIN_CAPACITY);
  bool ok = block_splittable(block, aligned_query);
  if (ok) {
    block_size old_size = size_from_capacity(block->capacity);
    block_size size = size_from_capacity( (block_capacity){ .bytes = aligned_query } );
    block_init(block, size, block->next);
    struct block_header* splitted = block_after(block);
    block_init(splitted, (block_size){ .bytes = old_size.bytes - size.bytes }, block->next);
    block->next = splitted;
  }
  return ok;
}


static void* block_after( struct block_header const* block ) {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) { // ---------------------------------- stage 3
  bool ok = NULL != block && NULL != block->next && mergeable(block, block->next);
  if (ok) {
    block_size size = size_from_capacity( block->capacity );
    block_size next_size = size_from_capacity( block->next->capacity );
    block_init(block, (block_size){ .bytes = size.bytes + next_size.bytes }, block->next->next);
  }
  return ok;
}


struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    { // ---------------------------------- stage 4
  struct block_header* current = block;
  while (NULL != current) {
    if (current->is_free && current->capacity.bytes >= sz) {
      return (struct block_search_result) { .type = BSR_FOUND_GOOD_BLOCK, .block = current };
    }
    if (NULL == current->next) {
      return (struct block_search_result) { .type = BSR_REACHED_END_NOT_FOUND, .block = current };
    }
    if (!try_merge_with_next(current)) {
      current = current->next;
    }
  }

  return (struct block_search_result) { .type = BSR_CORRUPTED, .block = NULL };
}

static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {  // ---------------------------------- stage 6
  struct block_search_result result = find_good_or_last(block, query);
  if (BSR_FOUND_GOOD_BLOCK == result.type) {
     if (split_if_too_big(result.block, query)) {
         try_merge_with_all_next(result.block->next);
     }
     result.block->is_free = false;
  }
  return result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) { // ---------------------------------- stage 2
  const void* hint = block_after(last);
  const struct region region = alloc_region(hint, query);

  struct block_header* ret;
  if (NULL == region.addr) {
    ret = NULL;
  } else if (hint == region.addr && last->is_free) {
    last->next = region.addr;
    ret = try_merge_with_next (last) ? last : NULL;
  } else {
    block_init(region.addr, (block_size){ .bytes = region.size }, NULL);
    ret = region.addr;
    last->next = ret;
  }
  return ret;
}

static void try_merge_with_all_next(struct block_header* block) {
  struct block_header* current = block;
  while (try_merge_with_next(current));
}

static struct block_header* memalloc( size_t query, struct block_header* heap_start) {  // ---------------------------------- stage 7
  struct block_search_result result = try_memalloc_existing(query, heap_start);
  switch (result.type) {
    case BSR_FOUND_GOOD_BLOCK:
        return result.block;
    case BSR_REACHED_END_NOT_FOUND: {
        struct block_header* new_block = grow_heap(result.block, query);
        if (NULL == new_block) {
            return NULL;
        }
        split_if_too_big(new_block, query);
        new_block->is_free = false;
        return new_block;
    }
    case BSR_CORRUPTED:
        err("Heap corruption detected");
    default:
        err("Unexpected block search result: %s", result.type);
  }
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) { // ---------------------------------- stage 3
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while (try_merge_with_next(header));
}
